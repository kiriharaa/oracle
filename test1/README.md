软工四班  学号：202010414420   姓名：吴爽
# 实验1：SQL语句的执行计划分析与优化指导

#实验目的



分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。
```

# 实验内容


对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，
通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，
看看该工具有没有给出优化建议。
```

# 查询语句


查询一
sqlplus hr/123@localhost/pdborcl

set autotrace on

SELECT d.department_name,
       count(e.job_id) as "部门总人数",
       avg(e.salary) as "平均工资"
FROM hr.departments d, hr.employees e
WHERE d.department_id = e.department_id
  AND d.department_name in ('IT','Sales')
GROUP BY d.department_name;


输出结果：

部门总人数   平均工资
-------   --------
IT           5       5760
Sales        34      8955

Predicate Information (identified by operation id):
---------------------------------------------------
   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
Note
-----
   - this is an adaptive plan


统计信息
-------------------------------------------------------
	  0  recursive calls
	  0  db block gets
	 10  consistent gets
	  0  physical reads
	  0  redo size
	815  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

分析
这个查询语句使用了 Oracle 数据库的 SQL 语言，目的是查询部门名为“IT”和“Sales”的总人数和平均工资。查询使用了内联结和GROUP BY子句来组织和计算数据。通过设置AUTOTRACE ON选项，该查询还提供了关于查询执行计划和统计信息的详细信息。
```
查询二
```
set autotrace on

SELECT d.department_name, COUNT(e.job_id) AS "部门总人数", AVG(e.salary) AS "平均工资"
FROM hr.departments d
INNER JOIN hr.employees e ON d.department_id = e.department_id
WHERE d.department_name IN ('IT', 'Sales')
GROUP BY d.department_name
HAVING d.department_name IN ('IT', 'Sales');


输出结果：

部门总人数   平均工资
-------   --------
IT           5       5760
Sales        34      8955

Predicate Information (identified by operation id):
---------------------------------------------------
   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
Note
-----
   - this is an adaptive plan



统计信息
------------------------------------------------------
	  0  recursive calls
	  0  db block gets
	  9  consistent gets
	  0  physical reads
	  0  redo size
	815  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

分析
```
该查询从 hr.departments 表和 hr.employees 表中检索部门名称、部门总人数和平均工资，并且只返回部门名称为 "IT" 或 "Sales" 的记录。该查询使用了INNER JOIN（等值连接）来连接两个表，使用 GROUP BY 对结果进行分组，并使用 HAVING 对分组后的结果进行筛选。
```
优化代码
```
set autotrace on

SELECT d.department_name, COUNT(e.job_id) AS "部门总人数", AVG(e.salary) AS "平均工资"
FROM hr.departments d, hr.employees e
WHERE e.department_id = d.department_id
  AND d.department_id IN (
    SELECT department_id 
    FROM hr.departments 
    WHERE department_name IN ('IT', 'Sales')
  )
GROUP BY d.department_name;
```
输出结果：
-----------------------------------------------------------
部门总人数   平均工资
-------   --------
IT           5       5760
Sales        34      8955

Predicate Information (identified by operation id):
---------------------------------------------------
   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
Note
-----
   - this is an adaptive plan
分析：优化后的查询使用了子查询代替了 HAVING 子句中的条件筛选，从而避免了使用 HAVING 子句对结果集进行二次筛选的开销。
查询中的子查询从 hr.departments 表中获取部门名称为 "IT" 或 "Sales" 的部门的 department_id。子查询可以将查询结果缓存到内存中，避免了重复执行查询的开销。主查询使用 IN 子句将子查询的结果集作为筛选条件，只保留符合条件的行，从而避免了全表扫描的开销。