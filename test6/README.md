﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010414420    姓名：吴爽

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 步骤

#### 1.创建表空间

```
CREATE TABLESPACE system_tablespace DATAFILE 'system_tablespace.dbf' SIZE 100M;
CREATE TABLESPACE user_tablespace DATAFILE 'user_tablespace.dbf' SIZE 100M;

```
![image-20230526153959239](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526153959239.png)


#### 2.创建表
```
-- 创建用户表
CREATE TABLE user_table (
user_id NUMBER PRIMARY KEY,
username VARCHAR2(50),
password VARCHAR2(50),
email VARCHAR2(100),
phone_number VARCHAR2(20)
);

```

![image-20230526154022932](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154022932.png)

```
-- 创建商品表
CREATE TABLE product (
  product_id NUMBER PRIMARY KEY,
  product_name VARCHAR2(100),
  product_description VARCHAR2(500),
  product_price NUMBER,
  product_stock NUMBER
);

```

![image-20230526154035236](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154035236.png)  

```
-- 创建订单表
CREATE TABLE order_table (
  order_id NUMBER PRIMARY KEY,
  user_id NUMBER REFERENCES user_table(user_id),
  product_id NUMBER REFERENCES product(product_id),
  order_date DATE,
  order_quantity NUMBER,
  order_amount NUMBER
);

```

![image-20230526154055393](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154055393.png)

```
-- 创建库存表
CREATE TABLE inventory (
  product_id NUMBER PRIMARY KEY,
  FOREIGN KEY (product_id) REFERENCES product(product_id),
  current_stock NUMBER
);

```

![image-20230526154108367](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154108367.png)


#### 3.插入数据
```
-- 向user_table表插入10万条数据
DECLARE
v_counter NUMBER := 1;
BEGIN
WHILE v_counter <= 100000 LOOP
INSERT INTO user_table (user_id, username, email)
VALUES (v_counter, 'User ' || v_counter, 'user' || v_counter || '@example.com');
v_counter := v_counter + 1;
END LOOP;
COMMIT;
END;

```

![image-20230526154126021](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154126021.png)

```
-- 向product表插入10万条数据
DECLARE
  v_counter NUMBER := 1;
  BEGIN
    WHILE v_counter <= 100000 LOOP
    INSERT INTO product (product_id, product_name, product_description,product_price,product_stock)
    VALUES (v_counter, 'product_name ' || v_counter,'product_description ' || v_counter, v_counter * 10,v_counter * 100);
    v_counter := v_counter + 1;
  END LOOP;
  COMMIT;
END;

```

![image-20230526154145977](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154145977.png)

```
-- 向order_table表插入10万条数据
DECLARE
  v_counter NUMBER := 1;
  BEGIN
    WHILE v_counter <= 100000 LOOP
    INSERT INTO order_table (order_id, user_id, product_id, order_quantity, order_date,order_amount)
    VALUES (v_counter, FLOOR(DBMS_RANDOM.VALUE(1, 100000)), FLOOR(DBMS_RANDOM.VALUE(1, 100000)), FLOOR(DBMS_RANDOM.VALUE(1, 10)), SYSDATE,FLOOR(DBMS_RANDOM.VALUE(1, 1000)));
    v_counter := v_counter + 1;
  END LOOP;
  COMMIT;
END;xxxxxxxxxx --为订单表插入10w数据INSERT INTO TB_ORDER1 (order_id, product_id, quantity, price)SELECT-- 向order_table表插入10万条数据DECLARE  v_counter NUMBER := 1;  BEGIN    WHILE v_counter <= 100000 LOOP    INSERT INTO order_table (order_id, user_id, product_id, order_quantity, order_date,order_amount)    VALUES (v_counter, FLOOR(DBMS_RANDOM.VALUE(1, 100000)), FLOOR(DBMS_RANDOM.VALUE(1, 100000)), FLOOR(DBMS_RANDOM.VALUE(1, 10)), SYSDATE,FLOOR(DBMS_RANDOM.VALUE(1, 1000)));    v_counter := v_counter + 1;  END LOOP;  COMMIT;END;CONNECT BY level <= 20000;
```

![image-20230526154322863](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154322863.png)

#### 4.设计权限及用户分配

```
-- 创建管理员用户
CREATE USER admin IDENTIFIED BY pwd;
GRANT CONNECT, RESOURCE, DBA TO admin;
-- 创建普通用户
CREATE USER guest IDENTIFIED BY pwd;
GRANT CONNECT, RESOURCE TO guest;
GRANT SELECT, INSERT, UPDATE, DELETE ON user_table TO guest;
GRANT SELECT, INSERT, UPDATE, DELETE ON product  TO guest;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_table TO guest;
GRANT SELECT, INSERT, UPDATE, DELETE ON inventory TO guest;

```

![image-20230526154350427](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154350427.png)

#### 5.PL/SQL设计

1.创建程序包

```
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg AS
  -- 存储过程：创建订单
  PROCEDURE create_order(
    p_user_id IN NUMBER,
    p_product_id IN NUMBER,
    p_quantity IN NUMBER,
    p_order_date IN DATE
  );
  -- 存储过程：更新库存
  PROCEDURE update_inventory(
    p_product_id IN NUMBER,
    p_quantity IN NUMBER
  );
  -- 函数：计算订单总金额
  FUNCTION calculate_order_amount(
    p_product_price IN NUMBER,
    p_quantity IN NUMBER
  ) RETURN NUMBER;
END sales_pkg;

```

![image-20230526154408633](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154408633.png)

2.函数设计

```
-- 函数设计：创建订单
CREATE OR REPLACE PACKAGE BODY sales_pkg AS

  PROCEDURE create_order(
    p_user_id IN NUMBER,
    p_product_id IN NUMBER,
    p_quantity IN NUMBER,
    p_order_date IN DATE
  ) AS
    v_order_id NUMBER;
    v_product_price NUMBER;
    v_order_amount NUMBER;
  BEGIN
    -- 生成订单ID
    SELECT order_sequence.NEXTVAL INTO v_order_id FROM DUAL;

    -- 获取商品价格
    SELECT product_price INTO v_product_price FROM product WHERE product_id = p_product_id;

    -- 计算订单总金额
    v_order_amount := calculate_order_amount(v_product_price, p_quantity);

    -- 插入订单记录
    INSERT INTO order_table (order_id, user_id, product_id, order_date, order_quantity, order_amount)
    VALUES (v_order_id, p_user_id, p_product_id, p_order_date, p_quantity, v_order_amount);

    -- 更新库存
    update_inventory(p_product_id, p_quantity);
  END create_order;

  PROCEDURE update_inventory(
    p_product_id IN NUMBER,
    p_quantity IN NUMBER
  ) AS
    v_current_stock NUMBER;
  BEGIN
    -- 获取当前库存
    SELECT current_stock INTO v_current_stock FROM inventory WHERE product_id = p_product_id;

    -- 更新库存
    v_current_stock := v_current_stock - p_quantity;

    -- 更新库存表
    UPDATE inventory SET current_stock = v_current_stock WHERE product_id = p_product_id;
  END update_inventory;

  FUNCTION calculate_order_amount(
    p_product_price IN NUMBER,
    p_quantity IN NUMBER
  ) RETURN NUMBER AS
    v_order_amount NUMBER;
  BEGIN
    -- 计算订单总金额
    v_order_amount := p_product_price * p_quantity;

    RETURN v_order_amount;
  END calculate_order_amount;

END sales_pkg;
/

```

!![image-20230526154428928](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154428928.png)



#### 6.备份方案

> ```bash
> #!/bin/bash
> # 设置备份目录和文件名
> BACKUP_DIR="/path/to/backup"
> BACKUP_FILE="database_backup_$(date +%Y%m%d).dmp"
> # 使用Oracle Data Pump进行全量备份
> expdp system/password@db_name DIRECTORY=backup_dir DUMPFILE=$BACKUP_DIR/$BACKUP_FILE FULL=Y
> # 检查备份是否成功
> if [ $? -eq 0 ]; then
> echo "数据库备份成功：$BACKUP_DIR/$BACKUP_FILE"
> else
> echo "数据库备份失败"
> fi
> # 删除过期的备份文件
> find $BACKUP_DIR -type f -name 'database_backup_*' -mtime +30 -exec rm {} \;
> 
> ```
>
> ![image-20230526154501576](C:\Users\asus\AppData\Roaming\Typora\typora-user-images\image-20230526154501576.png)
>
> 1. 定期备份策略：
>    - 使用cron定时任务执行备份脚本，例如每周日凌晨进行全量备份。
> 2. 存储设备和位置：
>    - 将备份文件保存在指定的备份目录（`/path/to/backup`）中。
> 3. 备份周期和保留策略：
>    - 每周进行一次全量备份，保留最近30天的备份文件。
>    - 在备份脚本中使用`find`命令，删除30天前的备份文件。
> 4. 备份文件的安全性：
>    - 可以在脚本中添加加密备份文件的步骤，使用加密工具（如GPG）对备份文件进行加密。
> 5. 监控和日志记录：
>    - 在备份脚本中添加日志记录，将备份操作的详细信息写入日志文件。
> 6. 灾难恢复策略：
>    - 根据需要，编写数据库恢复脚本，以从备份文件中还原数据库。
>
> 数据库备份在商品销售系统中至关重要，它提供了数据保护、业务连续性、灾难恢复、合规性和测试等多重好处，确保系统的可靠性、安全性和可用性，定期的备份策略是维护和管理数据库的重要组成部分。

### 总结

在这个实验中，我们设计了一个基于Oracle数据库的商品销售系统的数据库设计方案。以下是总结：

\1. 数据库设计方案：

  \- 创建了用户表（user_table），包含用户ID、用户名、密码、电子邮件和电话号码等字段。

  \- 创建了商品表（product），包含商品ID、商品名称、商品描述、商品价格和商品库存等字段。

  \- 创建了订单表（order_table），包含订单ID、用户ID、商品ID、订单日期、订单数量和订单金额等字段。

  \- 创建了库存表（inventory），包含商品ID和当前库存数量等字段。

 

\2. 权限及用户分配方案：

  \- 创建了两个用户（sales_user1和sales_user2），并分配了CONNECT和RESOURCE权限。

  \- 授权了用户对相应表的SELECT、INSERT、UPDATE和DELETE操作权限。

\3. 存储过程和函数设计：

  \- 创建了程序包（sales_pkg），其中包含了创建订单的存储过程（create_order）、更新库存的存储过程（update_inventory）以及计算订单总金额的函数（calculate_order_amount）。

  \- 这些存储过程和函数实现了复杂的业务逻辑，通过调用它们可以实现订单的创建、库存的更新和订单金额的计算。

\4. 备份方案：

  \- 在实际应用中，数据库的备份是非常重要的，以确保数据的安全性和可恢复性。

  \- 可以使用Oracle数据库的备份工具（如RMAN）进行定期的数据库备份，并将备份文件存储在可靠的位置，以防止数据丢失或意外损坏。

通过完成这个实验，我们学习了如何设计一个基于Oracle数据库的商品销售系统，并了解了数据库的表设计、用户权限分配、存储过程和函数的使用，以及数据库备份的重要性。这些知识对于构建实际的商业应用系统非常有价值。